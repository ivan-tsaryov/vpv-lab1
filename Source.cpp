#include <stdio.h>
#include <iostream>
#include <math.h>
#include <conio.h>
#include <time.h> 

using namespace std;

typedef float (*PFLOAT)(float);

const float a = 0.14f;

float fact(int n) {
	if (n < 0)
		return 0;
	if (n == 0)
		return 1;
	else
		return n * fact(n - 1);
}

float coeffs[10] = { cos(a) / fact(0), -sin(a) / fact(1), -cos(a) / fact(2), sin(a) / fact(3), cos(a) / fact(4),
-sin(a) / fact(5), -cos(a) / fact(6), sin(a) / fact(7), cos(a) / fact(8), -sin(a) / fact(9)
};

float coeffs_[10] = { cos(a) / fact(0), sin(a) / fact(1), cos(a) / fact(2), sin(a) / fact(3), cos(a) / fact(4),
sin(a) / fact(5), cos(a) / fact(6), sin(a) / fact(7), cos(a) / fact(8), sin(a) / fact(9)
};

bool flverify(PFLOAT  p, bool is_fl) {
	float Err = pow(2.0f, -20);
	float x, step = pow(2.0f, -21);

	float xgr1 = 0, xgr2 = 1;
	bool ok = true;

	for (ok = true, x = 0; x < xgr2; x += step) {
		if (is_fl) {
			if (abs(p(x) - cos(x + a)) > Err) {
				ok = false;
			}
		} else {

		}
	}
	return ok;
}

int time_det(PFLOAT fun, bool is_fl) {
	float x = 0.5;
	if (is_fl) {
		int start = clock();
		int i, j;
		double Mil = 100;
		for (i = 0; clock() - start < 100; i++) fun(x);
		double t_fun = Mil / i; // dirt time
		start = clock();
		for (j = 0; clock() - start < 100; j++);
		double t_cylc = Mil / j;
		return (int)((t_fun - t_cylc)/CLOCKS_PER_SEC*1000000000);
	}
	return 0;
}

float FlCyclNoGorner(float x) {
	float y = 0;
	int flag = 1;

	for (int i = 0; i < 10; i += 2) {
		y += pow(x, i)*flag*cos(a) / fact(i);
		flag *= -1;
		y += pow(x, i + 1)*flag*sin(a) / fact(i + 1);
	}
	return y;
}

float FlCyclGorner(float x) {
	
	float y = coeffs_[9];

	for (int i = 8; i >= 0; i--)
		y = y*x*pow(-1.0f, i + 1) + coeffs_[i];

	return y;
}

inline float FlNoCyclGorner(float x) {
	return ((((((((coeffs[9] * x + coeffs[8])*x + coeffs[7])*x + coeffs[6])*x + coeffs[5])*x + coeffs[4])*x
		+ coeffs[3])*x + coeffs[2])*x + coeffs[1])*x + coeffs[0];
}

float FlNoCyclNoGorner(float x) {
	/*float coeffs[10] = { cos(a) / fact(0), -sin(a) / fact(1), -cos(a) / fact(2), sin(a) / fact(3), cos(a) / fact(4),
		-sin(a) / fact(5), -cos(a) / fact(6), sin(a) / fact(7), cos(a) / fact(8), -sin(a) / fact(9)
	};
*/
	return coeffs[0] + x*coeffs[1] + pow(x, 2)*coeffs[2] + pow(x, 3)*coeffs[3] + pow(x, 4)*coeffs[4] + pow(x, 5)*coeffs[5]
		+ pow(x, 6)*coeffs[6] + pow(x, 7)*coeffs[7] + pow(x, 8)*coeffs[8] + pow(x, 9)*coeffs[9];
}

struct rel {
	PFLOAT fun;
	bool is_fl;
	char* name_fun;
	int time_f;  
};

int main() {
	rel list_fu[4] = { 
						{ FlCyclNoGorner, true, "FlCyclNoGorner" }, 
						{ FlCyclGorner, true, "FlCyclGorner" }, 
						{ FlNoCyclNoGorner, true, "FlNoCyclNoGorner" },
						{ FlNoCyclGorner, true, "FlNoCyclGorner" },
						//{ FixCyclNoGorner, false, "FixCyclNoGorner" },
						//{ FixCyclGorner, false, "FixCyclGorner" },
						//{ FixNoCyclNoGorner, false, "FixNoCyclNoGorner" },
						//{ FixNoCyclGorner, false, "FixNoCyclGorner" }
	};

	for (int i = 0; i < sizeof(list_fu) / sizeof(rel); i++) {
		if (!flverify(list_fu[i].fun, list_fu[i].is_fl)) {
			cout << "error in " << list_fu[i].name_fun << endl;
			exit(0);
		}
	}
	for (int i = 0; i < sizeof(list_fu) / sizeof(rel); i++) {
		list_fu[i].time_f = time_det(list_fu[i].fun, list_fu[i].is_fl);
		cout << list_fu[i].name_fun << " - " << list_fu[i].time_f << " HC" << endl;
	}

	//_getch();
	return 0;
}
